"use strict";
const path = require("path");
/** @type Egg.EggPlugin */
module.exports = {
  // had enabled by egg
  // static: {
  //   enable: true,
  // }
  ejs: {
    enable: true,
    package: "egg-view-ejs",
  },
  //暴露自己设置的插件
  auth: {
    enable: true,
    path: path.join(__dirname, "../lib/plugin/egg-auth"),
  },
  info: {
    enable: true,
    path: path.join(__dirname, "../lib/plugin/egg-info"),
  },
  allowHosts: {
    enable: true,
    path: path.join(__dirname, "../lib/plugin/egg-allowHosts"),
  },
  interfaceCache: {
    enable: true,
    path: path.join(__dirname, "../lib/plugin/egg-interfaceCache"),
  },
  mysql: {
    enable: true,
    package: "egg-mysql",
  },
  sequelize: {
    enable: true,
    package: "egg-sequelize",
  },
  jwt: {
    enable: true,
    package: "egg-jwt",
  },
  redis: {
    enable: true,
    package: "egg-redis",
  },
  cors: {
    enable: true,
    package: "egg-cors",
  },
  notFound: {
    enable: true,
    path: path.join(__dirname, "../lib/plugin/egg-notFound"),
  },
  nunjucks: {
    enable: true,
    package: "egg-view-nunjucks",
  },
  assets: {
    enable: true,
    package: "egg-view-assets",
  },
};
