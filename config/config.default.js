/* eslint valid-jsdoc: "off" */

"use strict";

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
const path = require("path");
module.exports = (appInfo) => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {});
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + "_1637245408974_2663";
  config.jwt = {
    secret: "cjingj",
  };
  config.session = {
    key: "MUKE_SESS",
    httpOnly: true,
    //session过期时间为一年
    maxAge: 1000 * 60 * 60 * 24 * 365,
    renew: true,
  };
  config.allowHosts = [
    "localhost:8000",
    "localhost:7001",
    "127.0.0.1:7001",
    "127.0.0.1:8000",
    "139.9.73.186:7001",
    "139.9.73.186",
    "192.168.3.15:8000",
  ];
  config.interfaceCache = {
    expire: 10,
    include: ["/api/user/detail"],
  };
  config.view = {
    mapping: {
      ".html": "nunjucks",
    },
    defaultViewEngine: "nunjucks",
    root: [
      path.join(appInfo.baseDir, "app/html"),
      path.join(appInfo.baseDir, "app/view"),
    ].join(","),
  };
  config.ejs = {};
  //先关掉csrf防御
  config.security = {
    csrf: {
      enable: false,
      ignoreJSON: true,
    },
    domainWhiteList: [
      "http://localhost:8000",
      "http://127.0.0.1:8000",
      "http://139.9.73.18:7001",
    ],
  };
  config.cors = {
    origin: "*",
    allowMethods: "GET,HEAD,PUT,POST,DELETE,PATCH",
    allowHeaders: "*",
    credentials: true,
  };
  // config.static = {
  //   prefix: "/assets/",
  //   dir: path.join(appInfo.baseDir, "app/public"),
  // };
  config.assets = {
    publicPath: "/public/",
    devServer: false,
    // env:['local']
  };
  config.auth = {
    exclude: [
      "/api/user/login",
      "/api/user/register",
      "/api/user/test",
      "/api/commons/citys",
      "/api/house/hot",
      "/index",
    ],
  };

  config.mysql = {
    app: true,
    agent: false,
    client: {
      host: "139.9.73.186",
      port: "3306",
      user: "root",
      password: "123456",
      database: "egg_house",
    },
  };
  config.redis = {
    client: {
      port: 6379,
      host: "139.9.73.186",
      password: "123456",
      db: 0,
    },
  };
  config.sequelize = {
    dialect: "mysql",
    host: "139.9.73.186",
    port: "3306",
    user: "root",
    password: "123456",
    database: "egg_house",
    define: {
      timestamps: false, //是否自动添加时间戳字段
      freezeTableName: true,
    },
  };
  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
    salt: "muke",
    redisExpire: 60 * 60 * 24 * 1000,
  };

  return {
    ...config,
    ...userConfig,
  };
};
