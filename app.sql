create database egg_house;

use egg_house;

--- 用户表
create table `user`(
  `id` int not null auto_increment,
  `username` varchar(20) default null comment '用户名',
  `password` varchar(64) default null comment '密码',
  `avatar` text comment '头像',
  `phone` varchar(20) default null comment '电话',
  `sign` varchar(300) default null comment '用户签名',
  `createTime` timestamp default null comment '创建时间',
  `updateTime` timestamp default null comment '更新时间',
  primary key(`id`)
)engine=InnoDB auto_increment=1 default charset=utf8 comment='用户表';

-- 民宿表
create table `house`(
  `id` int not null auto_increment,
  `name` varchar(50) default null comment '房屋名称',
  `info` varchar(150) default null comment '房屋简介',
  `addres` varchar(200) default null comment '房屋地址',
  `price` int default null comment '房屋价格',
  `publishTime` timestamp default null comment '发布时间',
  `cityCode` varchar(10) not null comment '城市编码',
  `showCount` int(5) not null default 0 comment '展示次数',
  `startTime` timestamp default null comment '开始出租时间',
  `endTime` timestamp default null comment '出租结束时间',
  primary key(`id`)
) engine=InnoDB auto_increment=1 default charset=utf8 comment='房屋表';

-- 图片表
create table `imgs`(
  `id` int not null auto_increment,
  `url` varchar(500) default null comment '图片地址',
  `houseId` int not null comment '房屋id',
  `createTime` timestamp default null comment '创建时间',
  primary key(`id`)
) engine=InnoDB auto_increment=1 default charset=utf8 comment='图片表';

-- 评论表
create table `comment`(
  `id` int not null auto_increment,
  `userId` int not null comment '用户表id',
  `houseId` int not null comment '房屋表id',
  `msg` varchar(500) default null comment '评论内容',
  `createTime` timestamp default null comment '创建时间',
  primary key(`id`)
) engine=InnoDB auto_increment=1 default charset=utf8 comment='评论表';

INSERT INTO `house` VALUES 
(1,'东城民宿','东区 临近地铁','东城区',200,'2021-08-10 13:37:57','10001',1,'2021-08-10 13:37:57','2021-10-20 13:37:57'),
(2,'西城民宿','西区 临近地铁','西城区',100,'2021-08-10 13:38:23','10001',1,'2021-08-10 13:37:57','2021-11-10 13:37:57'),
(3,'新区民宿','新区民宿位置优越','新城区',150,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-12-10 13:37:57'),
(4,'老城民宿','老城区风景秀美','老城区',100,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-12-10 13:37:57'),
(5,'西苑民宿','西苑风景秀美','西城区',100,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-11-10 13:37:57'),
(6,'紫金巷民宿','紧邻老城区风景秀美','东城区',120,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-11-10 13:37:57'),
(7,'北戴河民宿','风景秀美适合放松身心','北城区',100,'2021-08-10 13:38:23','10002',0,'2021-08-10 13:37:57','2021-12-10 13:37:57'),
(8,'南苑民宿','南苑民宿风景秀美','东城区',150,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-10-10 13:37:57'),
(9,'北苑民宿','北苑民宿风景秀美','北城区',100,'2021-08-10 13:38:23','10002',0,'2021-08-10 13:37:57','2021-12-10 13:37:57'),
(10,'三厢和民宿','紧邻地铁交通方便','东城区',200,'2021-08-10 13:38:23','10003',0,'2021-08-10 13:37:57','2021-10-10 13:37:57'),
(11,'老城区民宿','老城区风景秀美','老城区',100,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-12-10 13:37:57'),
(12,'老城区民宿','老城区风景秀美','老城区',100,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-12-10 13:37:57'),
(13,'老城区民宿','老城区风景秀美','老城区',100,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-12-10 13:37:57'),
(14,'老城区民宿','老城区风景秀美','老城区',100,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-12-10 13:37:57'),
(15,'老城区民宿','老城区风景秀美','老城区',100,'2021-08-10 13:38:23','10001',0,'2021-08-10 13:37:57','2021-12-10 13:37:57');

INSERT INTO `imgs` VALUES
(1,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F0171935e86854da80120a8951e5475.jpg%401280w_1l_2o_100sh.jpg&refer=http%3A%2F%2Fimg.zcool.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=6b5bf7c1431cddf317b9ce795271131f',1,'2021-08-11 13:37:57'),
(2,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpavo.elongstatic.com%2Fi%2Fmobile960%2Fnw_XzIOFRdZw4.jpg&refer=http%3A%2F%2Fpavo.elongstatic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=a5f8c3c99cf9289bcc77c637b30c24ef',1,'2021-08-11 13:37:57'),
(3,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg1.mydrivers.com%2Fimg%2F20210906%2Fs_61e83652e5a04d6ba3a101c69f2fee18.png&refer=http%3A%2F%2Fimg1.mydrivers.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=b2a829cca46f0200f8f9dff6107673d6',1,'2021-08-11 13:37:57'),
(4,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg1.mydrivers.com%2Fimg%2F20210712%2Fb12ab194-9da9-4981-8b18-29d501651d50.jpg&refer=http%3A%2F%2Fimg1.mydrivers.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=6fcbf8adcab9e802fbc0b833f51d9a98',2,'2021-08-11 13:37:57'),
(5,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fn.sinaimg.cn%2Fspider20210615%2F690%2Fw892h598%2F20210615%2F31d3-krpikqf1975722.png&refer=http%3A%2F%2Fn.sinaimg.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=f8ba3e050d73b961e0089ff20c7f6310',2,'2021-08-11 13:37:57'),
(6,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fak-d.tripcdn.com%2Fimages%2F200i0e00000070gme44D3_R_600_400_R5_D.jpg&refer=http%3A%2F%2Fak-d.tripcdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=215860ba7ade38f1ec96dc76477a6472',3,'2021-08-11 13:37:57'),
(7,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F0171935e86854da80120a8951e5475.jpg%401280w_1l_2o_100sh.jpg&refer=http%3A%2F%2Fimg.zcool.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=6b5bf7c1431cddf317b9ce795271131f',4,'2021-08-11 13:37:57'),
(8,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpavo.elongstatic.com%2Fi%2Fmobile960%2Fnw_XzIOFRdZw4.jpg&refer=http%3A%2F%2Fpavo.elongstatic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=a5f8c3c99cf9289bcc77c637b30c24ef',5,'2021-08-11 13:37:57'),
(9,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg1.mydrivers.com%2Fimg%2F20210906%2Fs_61e83652e5a04d6ba3a101c69f2fee18.png&refer=http%3A%2F%2Fimg1.mydrivers.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=b2a829cca46f0200f8f9dff6107673d6',6,'2021-08-11 13:37:57'),
(10,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg1.mydrivers.com%2Fimg%2F20210712%2Fb12ab194-9da9-4981-8b18-29d501651d50.jpg&refer=http%3A%2F%2Fimg1.mydrivers.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=6fcbf8adcab9e802fbc0b833f51d9a98',7,'2021-08-11 13:37:57'),
(11,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fn.sinaimg.cn%2Fspider20210615%2F690%2Fw892h598%2F20210615%2F31d3-krpikqf1975722.png&refer=http%3A%2F%2Fn.sinaimg.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=f8ba3e050d73b961e0089ff20c7f6310',8,'2021-08-11 13:37:57'),
(12,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fak-d.tripcdn.com%2Fimages%2F200i0e00000070gme44D3_R_600_400_R5_D.jpg&refer=http%3A%2F%2Fak-d.tripcdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=215860ba7ade38f1ec96dc76477a6472',9,'2021-08-11 13:37:57'),
(13,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F0171935e86854da80120a8951e5475.jpg%401280w_1l_2o_100sh.jpg&refer=http%3A%2F%2Fimg.zcool.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=6b5bf7c1431cddf317b9ce795271131f',10,'2021-08-11 13:37:57'),
(14,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpavo.elongstatic.com%2Fi%2Fmobile960%2Fnw_XzIOFRdZw4.jpg&refer=http%3A%2F%2Fpavo.elongstatic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=a5f8c3c99cf9289bcc77c637b30c24ef',11,'2021-08-11 13:37:57'),
(15,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg1.mydrivers.com%2Fimg%2F20210906%2Fs_61e83652e5a04d6ba3a101c69f2fee18.png&refer=http%3A%2F%2Fimg1.mydrivers.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=b2a829cca46f0200f8f9dff6107673d6',12,'2021-08-11 13:37:57'),
(16,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg1.mydrivers.com%2Fimg%2F20210712%2Fb12ab194-9da9-4981-8b18-29d501651d50.jpg&refer=http%3A%2F%2Fimg1.mydrivers.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=6fcbf8adcab9e802fbc0b833f51d9a98',13,'2021-08-11 13:37:57'),
(17,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fn.sinaimg.cn%2Fspider20210615%2F690%2Fw892h598%2F20210615%2F31d3-krpikqf1975722.png&refer=http%3A%2F%2Fn.sinaimg.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=f8ba3e050d73b961e0089ff20c7f6310',14,'2021-08-11 13:37:57'),
(18,'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fak-d.tripcdn.com%2Fimages%2F200i0e00000070gme44D3_R_600_400_R5_D.jpg&refer=http%3A%2F%2Fak-d.tripcdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1641903007&t=215860ba7ade38f1ec96dc76477a6472',15,'2021-08-11 13:37:57')
;
-- 订单表
create table `orders`(
  `id` int not null auto_increment,
  `orderNumber` varchar(20) default null comment '订单编号',
  `userId` int not null comment '用户id',
  `houseId` int not null comment '房屋id',
  `isPayed` int default 0 comment '是否支付，0未支付，1已支付',
  `createTime` timestamp default null comment '创建时间',
  `updateTime` timestamp default null comment '更新时间',
  primary key(`id`)
)engine=InnoDB auto_increment=1 default charset=utf8 comment='订单表';