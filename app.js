module.exports = app => {

  const store = {};
  app.sessionStore = {
    async get(key) {
      return store[key];
    },
    async set(key, value, maxAge) {
      store[key] = value;
    },
    async destroy(key) {
      store[key] = null;
    }
  };
  //在这里引入中间件
  const mids = app.config.coreMiddleware;
  app.config.coreMiddleware = [...mids, ...[
    'allowHosts',
    'notFound',
    'auth',
    'interfaceCache'
  ]];

}