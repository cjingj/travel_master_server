# egg



## QuickStart

<!-- add docs here for user -->

see [egg docs][egg] for more detail.

### Development

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### Deploy

```bash
$ npm start
$ npm stop
```

### npm scripts

- Use `npm run lint` to check code style.
- Use `npm test` to run unit test.
- Use `npm run autod` to auto detect dependencies upgrade, see [autod](https://www.npmjs.com/package/autod) for more detail.


[egg]: https://eggjs.org
###  session切换成redis
auth 文件  和  controler/user 文件  同时修改即可
###  extend
扩展方法文件，类似于lib库
###  moddleware
中间件
引入方法
1.app/middleware/xx.js 然后去config.default.js  配置config.middleware = ['httpLog'];最后
挂在app.config.appMiddleware
2.  在lib文件夹下写好插件，使用package.json 暴露，然后去plugin引入，最终去app.js 使用 app.config.coreMiddleware.push('auth');