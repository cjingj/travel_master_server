module.exports = {
    //获取请求的参数，不传key则返回对象
    params(key){
      const method = this.request.method;
      if (method === 'GET') {
        return key ? this.query[key] : this.query;
      }else {
        return key ? this.request.body[key] : this.request.body;
      }
    },
    get username(){
      const token = this.request.header.token;
      //对token进行解密
      const tokenCache = token ? this.app.jwt.verify(token, this.app.config.jwt.secret) : undefined;
      return tokenCache ? tokenCache.username : undefined;
    },
    get userId(){
      const token = this.request.header.token;
      const tokenCache = token ? this.app.jwt.verify(token, this.app.config.jwt.secret) : undefined;
      return tokenCache ? tokenCache.id : undefined;
    }
  };