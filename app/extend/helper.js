const dayjs = require('dayjs');
//有点类似于前端的lib文件夹，一些常用函数工具库
module.exports = {
  base64Encode(str = ''){
    return new Buffer(str).toString('base64');
  },
  //转成时间格式
  time(){
    return dayjs().format('YYYY-MM-DD HH:mm:ss');
  },
  //日期转时间戳
  timestamp(data){
    return new Date(data).getTime();
  }, 
  //返回source里面arr没有的
  unPick(source, arr){
    if(Array.isArray(arr)){
      let obj = {};
      for(let i in source){
        if(!arr.includes(i)){
          obj[i] = source[i];
        }
      }
      return obj;
    }
  }
};