const Controller = require('egg').Controller;
const BaseController = require('./base');

class CommonsController extends BaseController {
    async citys() {
        const { ctx, app } = this;
        try {
            this.success(
                [[
                    { label: '杭州', value: '10001' },
                    { label: '苏州', value: '10002' },
                    { label: '上海', value: '10003' },
                    { label: '绍兴', value: '10004' },
                    { label: '大同', value: '10005' },
                    { label: '嘉兴', value: '10006' },
                    { label: '芜湖', value: '10007' },
                    { label: '上尧', value: '10008' }
                ]]
            );

        } catch (error) {
            console.log('error------', error)
            this.error('获取城市数据失败');
        }

    }
}
module.exports = CommonsController;