const Controller = require('egg').Controller;
const BaseController = require('./base');

class OrdersController extends BaseController {
    //查看该订单状态，以 userId,houseId去查
    async hasOrder() {
        const { ctx, app } = this;
        const result = await ctx.service.orders.hasOrder({
            userId: ctx.userId,
            houseId: ctx.params('id')
        });

        this.success(result);
    }
    //添加订单，以 userId,houseId,为单位
    async addOrder() {
        const { ctx, app } = this;
        const result = await ctx.service.orders.addOrder({
            userId: ctx.userId,
            houseId: ctx.params('id'),
            isPayed: 0,
            createTime: ctx.helper.time()
        });

        this.success(result);
    }
    //取消预定，根据订单的id
    async delOrder() {
        const { ctx, app } = this;
        const result = await ctx.service.orders.delOrder(ctx.params('id'));

        this.success(result);
    }
    async lists() {
        const { ctx, app } = this;
        const result = await ctx.service.orders.lists({
            ...ctx.params(),
            userId: ctx.userId
        });

        this.success(result);
    }
    // 返回订单编号拼接时间戳
    async invokePay(params) {
        return {
            orderNumber: params.id + new Date().getTime()
        }
    }
    async pay() {
        const { ctx, app } = this;
        const { id } = ctx.params();
        const order = await ctx.model.Orders.findByPk(id)
        if (order) {
            try {
                const beforePay = await this.invokePay({ id });
                const result = await ctx.service.orders.pay({
                    id,
                    orderNumber: beforePay.orderNumber
                });
                this.success(result);
            } catch (error) {
                this.error('订单支付失败');
            }
        } else {
            this.error('订单不存在')
        }
    }
}
module.exports = OrdersController;