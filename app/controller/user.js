"use strict";
const Controller = require("egg").Controller;
const md5 = require("md5");
const dayjs = require("dayjs");
const BaseController = require("./base");
class UserController extends BaseController {
  async jwtSign({ id, username }) {
    const { ctx, app } = this;
    const token = app.jwt.sign(
      {
        id,
        username,
      },
      app.config.jwt.secret
    );
    //设置这个session作为每个需要登录态的请求接口作为检查，具体逻辑在egg-auth/app/middleware/auth.js文件
    // ctx.session[username] = 1;
    //存进去的是token，key为username
    await app.redis.set(username, token, "EX", app.config.redisExpire);
    return token;
  }
  //抽象方法，此方法默认去除ctx对象的password属性，增加createTime
  parseResult(ctx, result) {
    return {
      ...ctx.helper.unPick(result.dataValues, ["password"]),
      createTime: ctx.helper.timestamp(result.createTime),
    };
  }
  async register() {
    const { ctx, app } = this;
    const params = ctx.params();
    const user = await ctx.service.user.getUser(params.username);
    if (user) {
      this.error("用户已经存在");
      return;
    }
    const result = await ctx.service.user.add({
      ...params,
      password: md5(params.password + app.config.salt),
      createTime: ctx.helper.time(),
    });
    if (result) {
      const token = await this.jwtSign({
        id: result.id,
        username: result.username,
      });
      this.success({
        ...this.parseResult(ctx, result),
        token,
      });
    } else {
      this.error("注册使用失败");
    }
  }
  async login() {
    const { ctx, app } = this;
    const { username, password } = ctx.params();
    const user = await ctx.service.user.getUser(username, password);
    if (user) {
      //对用户名进行加密
      const token = await this.jwtSign({
        id: user.id,
        username: user.username,
      });
      this.success({
        ...this.parseResult(ctx, user),
        token,
      });
    } else {
      this.error("该用户不存在");
    }
  }
  async detail() {
    const { ctx } = this;
    const user = await ctx.service.user.getUser(ctx.username);
    if (user) {
      this.success({
        ...this.parseResult(ctx, user),
      });
    } else {
      this.error("该用户不存在");
    }
  }
  async logout() {
    const { ctx } = this;
    try {
      ctx.session[ctx.username] = null;
      this.success("ok");
    } catch (error) {
      this.error("退出登录失败");
    }
  }
  async edit() {
    const { ctx, app } = this;
    const result = ctx.service.user.edit({
      ...ctx.params(),
      updateTime: ctx.helper.time(),
      sign: ctx.helper.escape(ctx.params("sign")),
    });
    await app.redis.del('/api/user/detail');
    this.success(result);
  }
  async test() {
    const { ctx, app } = this;
    ctx.body = "hello world";
  }
  async index() {
    const { ctx, app } = this;
    const htmlTemplate = await ctx.view.render("index.html");

    const serverRender = require("../public/umi.server");
    // 将 html 模板传到服务端渲染函数中
    const { html, error, rootContainer } = await serverRender({
      path: ctx.url,
      getInitialPropsCtx: {},
      htmlTemplate,
    });
    // console.log("html----------", html);
    // console.log("error------", error);
    // console.log("rootContainer----", rootContainer);
    if (error) {
      ctx.logger.error(
        "[SSR ERROR] 渲染报错，切换至客户端渲染",
        error,
        ctx.url
      );
    }
    ctx.type = "text/html";
    ctx.status = 200;
    ctx.body = html;
  }
}
module.exports = UserController;
