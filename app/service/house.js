const Service = require('egg').Service;
const BaseService = require('./base');

class HouseService extends BaseService {
    commonAttr(app) {
        return {
            //排序
            order: [
                ['showCount', 'DESC']
            ],
            //去除一些树形
            attributes: {
                exclude: ['startTime', 'endTime', 'publishTime']
            },
            //引入图片,只要一张
            include: [
                {
                    model: app.model.Imgs,
                    limit: 1,
                    attributes: ['url']
                }
            ]
        }
    }
    async hot() {
        return this.run(async (ctx, app) => {
            const result = await ctx.model.House.findAll({
                ...this.commonAttr(app),
                limit: 4
            })
            return result
        })
    }
    async search(params) {
        return this.run(async (ctx, app) => {
            const { lte, gte, like } = app.Sequelize.Op;
            const where = {
                //获取到的是否是数组
                cityCode: Array.isArray(params.code) ? params.code[0] : params.code,
                //lte 大于等于
                startTime: {
                    [lte]: params.startTime
                },
                //lte 小于等于
                endTime: {
                    [gte]: params.endTime
                },
                //模糊查询
                name: {
                    [like]: '%' + params.houseName + '%'
                }
            };
            if (!params.houseName) {
                delete where.name;
            }
            const result = await ctx.model.House.findAll({
                ...this.commonAttr(app),
                limit: 8,
                //数据偏移量
                offset: (params.pageNum - 1) * params.pageSize,
                where
            });

            return result;
        });
    }
    async detail(id) {
        return this.run(async (ctx, app) => {
            const result = await ctx.model.House.findOne({
                where: {
                    id
                },
                include: [
                    {
                        model: app.model.Imgs,
                        attributes: ['url']
                    }
                ]
            })
            await ctx.model.House.update({
                showCount: result.showCount + 1
            }, {
                where: {
                    id
                }
            })
            return result
        })
    }
}
module.exports = HouseService;