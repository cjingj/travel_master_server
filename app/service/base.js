const Service = require('egg').Service;
//封装try-catch
class BseService extends Service {
  run(callback) {
    const { ctx, app } = this;
    try {
      if(callback)
        return callback(ctx, app);
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}

module.exports = BseService;