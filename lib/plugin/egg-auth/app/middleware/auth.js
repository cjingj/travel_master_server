module.exports = options => {
  //在app.js引入这个中间件
  //这个中间件是检查每个需要登录验证的接口是否带了token过来
  // ，否则就返回1001让前端跳转到登录页面
  return async (ctx, next) => {
    const url = ctx.request.url;
    //因为写了extend所以可以直接获取到header的token
    const token = ctx.request.token
    //const user = ctx.session[ctx.username];
    //ctx.username 为token经过解密后拿出来的username
    const userToken = await ctx.app.redis.get(ctx.username)
    //首先判断redis中是否存在token，如果存在，就拿redis中的token和header中的token比较是否相等
    //如果redis中的userToken不存在则根据userToken返回true或false
    //因为每次登录后都会生成新的token，拿新的token去请求一些接口也是通过的，所以要做以下判断
    const user = userToken ? userToken === token : userToken;
    if (!user && !options.exclude.includes(ctx.request.url.split('?')[0])) {
      ctx.body = {
        status: 1001,
        errMsg: '用户未登录'
      };
    } else {
      await next();
    }
  }
}