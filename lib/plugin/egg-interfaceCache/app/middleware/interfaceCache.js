/**缓存接口
 * 1，接口地址作为redis中的key
 * 2，查询redis，有缓存，返回返回接口
 * 3，没有缓存，将接口返回结果保存到redis中
 */
module.exports = options => {
  return async (ctx, next) => {
    const { expire, include } = options
    const { url } = ctx.request;
    //看下redis有没有缓存到
    const cahce = await ctx.app.redis.get(url);
    //看看是否是需要缓存的url
    if (include.includes(url)) {
      //有就直接返回
      if (cahce) {
        ctx.body = JSON.parse(cahce);
        return;
        //首次缓存
      } else {
        await next();
        await ctx.app.redis.set(url, JSON.stringify(ctx.response.body), 'EX', expire);
      }
    } else {
      await next();
    }

  }
}